# from django.shortcuts import render
# from django.http import HttpResponseRedirect
# from django.db.models import Q
# from django.contrib import messages 
# from django.contrib.auth import authenticate, login, logout
# from django.contrib.auth.decorators import login_required
# from django.contrib.auth.models import User

# # Create your views here.
# from .models import Event
# from .forms import eventForm

# def index(request):
#     if request.method == 'POST':
#         srch = request.POST['srh']

#         if srch:
#             match = Event.objects.filter(Q(Nama_Event__icontains=srch) |
#                                              Q(Jenis_Event__icontains=srch) |
#                                              Q(Penyelenggara__icontains=srch) |
#                                              Q(Tanggal_Event__icontains=srch)
#                                              )
#             if match:
#                 return render(request, 'event/cari.html', {'sr':match, 'nama':srch})
#             else:
#                 messages.error(request, 'Hasil pencarian event tidak ditemukan, silahkan coba lagi')
#         else:
#             return HttpResponseRedirect('/event/cari/')

#     return render(request, 'event/cari.html')

# # @login_required
# def tambah(request):
#     event_form = eventForm(request.POST)
#     if request.method == 'POST':
#         if event_form.is_valid():
#             event_form.save()
#             return HttpResponseRedirect("/event/cari/")

#     context = {
#         'event_form' : event_form,
#     }
#     return render(request, 'event/tambah.html', context)
from django.shortcuts import render
from django.http import JsonResponse
import json
from .models import Event
from .forms import eventForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

def tambah(request):
    context = {}
    if request.user.is_authenticated:
        context['auth'] = True
        print("masuk")
        event_form = eventForm(request.POST)
        context['form'] = event_form
        if event_form.is_valid():
            event_form.save()
            return JsonResponse({
                'message': 'success'
            })
    else:
        context['notAuth'] = True
    return render(request, 'event/tambah.html', context)


def index(request):
    if request.method == 'POST':
        # print("masuk")
        srch = request.POST['srh']
        # print(srch)
        match = Event.objects.filter(
            Nama_Event__icontains=srch) | Event.objects.filter(
            Jenis_Event__icontains=srch) | Event.objects.filter(
            Penyelenggara__icontains=srch) | Event.objects.filter(
            Tanggal_Event__icontains=srch)
        data = match.values()
        # print(list(data))
        return JsonResponse(list(data), safe=False)
    return render(request, 'event/cari.html')