from django.urls import path

from . import views

app_name = 'event'

urlpatterns = [
    path('cari/', views.index, name='carievent'),
    path('tambah/', views.tambah, name='tambahevent'),
]