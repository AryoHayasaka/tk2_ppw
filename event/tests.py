from django.test import TestCase
from django.test import Client
from datetime import date, time
from .models import Event
from django.contrib.auth.models import User

class TestEvent(TestCase):
    def test_url_cari(self):
        response = Client().get('/event/cari/')
        self.assertEqual(response.status_code,200)

    def test_template_cari(self):
        response = Client().get('/event/cari/')
        self.assertTemplateUsed(response, 'event/cari.html')

    def test_url_tambah(self):
        response = Client().get('/event/tambah/')
        self.assertEqual(response.status_code,200)

    def test_template_tambah(self):
        response = Client().get('/event/tambah/')
        self.assertTemplateUsed(response, 'event/tambah.html')

    def test_check_cari_page_have_form(self):
        response = Client().get('/event/cari/')
        content = response.content.decode('utf8')
        self.assertIn('<form', content)

    def test_check_user_login_tambah_page(self):
        User.objects.create_user(username="xixixi", email="hihi@gmail.com", password="xixixi123")
        self.client.login(username="xixixi", password="xixixi123")
        response = self.client.get('/event/tambah/')
        content = response.content.decode('utf8')
        self.assertIn('Tambah Event', content)
    
    def test_check_user_notlogin_tambah_page(self):
        response = Client().get('/event/tambah/')
        content = response.content.decode('utf8')
        self.assertIn('Untuk dapat mengakses', content)

    def test_model_create_event(self):
        Event.objects.create(
            Jenis_Event='webinar', 
            Nama_Event = 'event_test',
            Penyelenggara = 'Kominfo',
            Tanggal_Event = date(2020, 11, 11),
            Waktu_Event = time(16, 00),
            Link_Pendaftaran = 'http://bkd.cilacapkab.go.id/p/562/webinar--seminar-online-di-masa-pandemi-covid-19'
        )
        total = Event.objects.all().count()
        self.assertEqual(total,1)

    def test_model_save_POST_request(self):
        User.objects.create_user(username="haxixi", email="hahihi@gmail.com", password="xixixi123")
        self.client.login(username="haxixi", password="xixixi123")
        response = self.client.post('/event/tambah/', data={
            'Jenis_Event' : 'seminar', 
            'Nama_Event' : 'kumps',
            'Penyelenggara' : 'SMAN A',
            'Tanggal_Event' : date(2020, 11, 21),
            'Waktu_Event' : time(17, 00),
            'Link_Pendaftaran' : 'http://bkd.cilacapkab.go.id/p/562/webinar--seminar-online-di-masa-pandemi-covid-19'
            })
        total = Event.objects.all().count()
        self.assertEqual(total,1)
        self.assertEqual(response.status_code, 200)

    def test_event_search(self):
        Event.objects.create(
            Jenis_Event='websss', 
            Nama_Event = 'test',
            Penyelenggara = 'Kominfo',
            Tanggal_Event = date(2021, 1, 2),
            Waktu_Event = time(12, 11),
            Link_Pendaftaran = 'http://bkd.cilacapkab.go.id/p/562/webinar--seminar-online-di-masa-pandemi-covid-19'
        )
        response = Client().post('/event/cari/', {'srh':'test'})
        responsenya = response.content.decode('utf8')
        self.assertIn('websss', responsenya)
        self.assertIn('test', responsenya)
        self.assertIn('Kominfo', responsenya)
        self.assertIn('2021-01-02', responsenya)
        self.assertIn('12:11:00', responsenya)


    def test_event_search_notfound(self):
        Event.objects.create(
            Jenis_Event='bersama', 
            Nama_Event = 'acara',
            Penyelenggara = 'presiden',
            Tanggal_Event = date(2020, 10, 31),
            Waktu_Event = time(10, 22),
            Link_Pendaftaran = 'http://bkd.cilacapkab.go.id/p/562/webinar--seminar-online-di-masa-pandemi-covid-19'
        )
        response = Client().get('/event/cari/', {'srh':'jiah'})
        responsenya = response.content.decode('utf8')
        self.assertIn('tidak ditemukan', responsenya)
