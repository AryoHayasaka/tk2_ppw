from django import forms

#import model dari models.py
from .models import Event

class eventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = [
            'Jenis_Event',
            'Nama_Event',
            'Penyelenggara',
            'Tanggal_Event',
            'Waktu_Event',
            'Link_Pendaftaran',
        ]
        widgets = {
            'Jenis_Event': forms.TextInput(  
                attrs={
                    'class' : 'form-control', #buat isi class sesuai bootstrapnya
                    'placeholder' : 'Ketik jenis event (ex. webinar)',
                }
            ),

            'Nama_Event': forms.TextInput(  
                attrs={
                    'class' : 'form-control',
                    'placeholder': 'Ketik nama event' #buat isi class sesuai bootstrapnya
                }
            ),

            'Penyelenggara': forms.TextInput(  
                attrs={
                    'class' : 'form-control',
                    'placeholder': 'Ketik penyelenggara event',
                    'border-radius': '30px'
                     #buat isi class sesuai bootstrapnya
                }
            ),
            
            'Tanggal_Event': forms.DateInput(  
                attrs={
                    'class' : 'form-control',
                    'type':'date',
                    'placeholder': 'Ketik tanggal event' #buat isi class sesuai bootstrapnya
                }
            ), 

            'Waktu_Event': forms.TimeInput(  
                attrs={
                    'class' : 'form-control',
                    'type':'time',
                    'placeholder': 'Ketik waktu event' #buat isi class sesuai bootstrapnya
                }
            ), 

            'Link_Pendaftaran': forms.TextInput(  
                attrs={
                    'class' : 'form-control',
                    'placeholder': 'Ketik link pendaftaran event' #buat isi class sesuai bootstrapnya
                }
            ),
        }