from django.db import models

# Create your models here.
class Study(models.Model):
    namaStudi = models.CharField(max_length = 150)
    TIPE_CHOICE = [
        ('Ekonomi', 'Ekonomi'),
        ('Sosial', 'Sosial'),
        ('Biologi', 'Biologi'),
        ('Budaya', 'Budaya')
    ]
    tipeStudi = models.CharField(max_length = 7, choices = TIPE_CHOICE)
    tanggalPublikasi = models.DateField()
    author = models.CharField(max_length = 50)
    abstract = models.TextField()
    link = models.CharField(max_length = 20)

