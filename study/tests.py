from django.test import TestCase, Client
from .models import Study
from .forms import StudyForm
from datetime import date
from django.contrib.auth.models import User

# Create your tests here.
class StudyTest(TestCase):
    def test_study_url_exist(self):
        response = Client().get('/studi/tambah/')
        self.assertEqual(200, response.status_code)

    def test_study_correct_template(self):
        response = Client().get('/studi/tambah/')
        self.assertTemplateUsed(response, 'study_add.html')

    def test_study_search_correct_template(self):
        response = Client().get('/studi/cari/')
        self.assertTemplateUsed(response, 'study_search.html')

    def test_study_model(self):
        Study.objects.create(namaStudi = 'lol', tipeStudi = 'Sosial', tanggalPublikasi = date(2020, 11, 11), author = 'hayasaka', abstract = 'lolololol', link = 'lol.com')
        cnt = Study.objects.all().count()
        self.assertEqual(1, cnt)

    def test_study_add_form(self):
        form = StudyForm(data={'namaStudi' : 'lol', 'tipeStudi' : 'Sosial', 'tanggalPublikasi' : date(2020, 11, 11), 'author' : 'hayasaka', 'abstract' : 'lolololol', 'link' : 'lol.com'})
        form.save()
        cnt = Study.objects.all().count()
        self.assertEqual(1, cnt)

    def test_study_search_get(self):
        Study.objects.create(namaStudi = 'lol', tipeStudi = 'Sosial', tanggalPublikasi = date(2020, 11, 11), author = 'hayasaka', abstract = 'lolololol', link = 'lol.com')
        response = Client().get('/studi/cari/response', {'query':'lol'})
        html_response = response.content.decode('utf8')
        self.assertIn('lol', html_response)
        self.assertIn('Sosial', html_response)
        self.assertIn('hayasaka', html_response)
        self.assertIn('lolololol', html_response)

    def test_study_search_get_notFound(self):
        Study.objects.create(namaStudi = 'lol', tipeStudi = 'Sosial', tanggalPublikasi = date(2020, 11, 11), author = 'hayasaka', abstract = 'lolololol', link = 'lol.com')
        response = Client().get('/studi/cari/', {'query':'lal'})
        html_response = response.content.decode('utf8')
        self.assertIn('Tidak Ditemukan', html_response)

    def test_study_add_post(self):
        self.dummyUser()
        self.client.login(username = 'Zhongli', password = '1234')
        response = self.client.post('/studi/tambah/post', {'namaStudi' : 'lol', 'tipeStudi' : 'Sosial', 'tanggalPublikasi' : date(2020, 11, 11), 'author' : 'hayasaka', 'abstract' : 'lolololol', 'link' : 'lol.com'})
        cnt = Study.objects.all().count()
        self.assertEqual(1, cnt)

    def test_study_add_logged_in(self):
        self.dummyUser()
        self.client.login(username = 'Zhongli', password = '1234')
        response = self.client.get('/studi/tambah/')
        html_response = response.content.decode('utf8')
        self.assertIn('<label for="nama">Nama Studi:</label>', html_response)
        self.assertIn('<label for="tipe">Tipe Studi:</label>', html_response)

    def test_study_add_not_logged_in(self):
        response = Client().get('/studi/tambah/')
        html_response = response.content.decode('utf8')
        self.assertIn('Anda harus login terlebih dahulu untuk menambahkan studi', html_response)


    def dummyUser(self):
        user = User.objects.create_user('Zhongli', 'rex@lapis.com', '1234')
    
