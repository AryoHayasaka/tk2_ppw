from django.urls import path
from .views import *

app_name = 'study'

urlpatterns = [
    path('tambah/', studyAddView, name='study'),
    path('cari/', studySearchView, name='searchstudy'),
    path('cari/response', get_study_request),
    path('tambah/post', post_study_request)
]