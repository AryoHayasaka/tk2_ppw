from django.shortcuts import render
from django.db.models import Q
from .forms import *
from .models import Study
from django.http import JsonResponse

# Create your views here.
def studyAddView(request):
    context = {}
    if request.user.is_authenticated:
        context['loggedin'] = True
        form = StudyForm()
        context['form'] = form
    else:
        context['notloggedin'] = True
    
    
    return render(request, 'study_add.html', context)

def post_study_request(request):
    nama = request.POST.get('namaStudi')
    tanggal = request.POST.get('tanggalPublikasi')
    author = request.POST.get('author')
    tipe = request.POST.get('tipeStudi')
    abstract = request.POST.get('abstract')
    link = request.POST.get('link')
    Study.objects.create(namaStudi = nama, 
                        tipeStudi = tipe, 
                        tanggalPublikasi = tanggal, 
                        author = author, 
                        abstract = abstract, 
                        link = link)
    response = {}
    
    
    return JsonResponse(data=response)

def studySearchView(request):
    return render(request, 'study_search.html')

def get_study_request(request):
    forJson = {'items' : []}
    query = request.GET.get('query')
    cari = Study.objects.filter(Q(namaStudi__icontains=query) |
                                       Q(tipeStudi__icontains=query) |
                                       Q(author__icontains=query) )
    if not cari:
        forJson['items'] = 'Tidak Ditemukan'
    else:
        for studi in cari:
            studiDict = {}
            studiDict['nama'] = studi.namaStudi
            studiDict['tipe'] = studi.tipeStudi
            studiDict['author'] = studi.author
            studiDict['tanggal'] = studi.tanggalPublikasi
            studiDict['abstract'] = studi.abstract
            studiDict['link'] = studi.link
            forJson['items'].append(studiDict)

    return JsonResponse(data=forJson)                             
