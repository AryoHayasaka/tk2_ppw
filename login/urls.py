from django.urls import path

from .views import *

app_name = 'login' 

urlpatterns = [
    path('', loginView, name='login'),
    path('register/', registerView, name='register'),    
    path('logout/', logoutView, name='logout'),    
]
