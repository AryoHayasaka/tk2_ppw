from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages

# Create your views here.
def loginView(request):

    if request.user.is_authenticated:
        return redirect('homepage:home')
    context = {}
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None :
            login(request, user)
            context['success'] = True
            return redirect('homepage:home')
        else :
            context['failed'] = True

    return render(request, 'login.html', context)

def registerView(request):
    context = {}
    if request.method == 'POST':
        email = request.POST.get('email')
        username = request.POST.get('username')
        password = request.POST.get('password')
        password1 = request.POST.get('passwordconfirm')
        if email != '' and username != '' and password != '' and password1 != '':
            valid = True
        else:
            context['notComplete'] = True
            valid = False

        user = User.objects.filter(username=username)
        if user.exists():
            context['userExist'] = True
            valid = False

        if password != password1:
            context['passwordBeda'] = True
            valid = False

        if valid:
            newUser = User.objects.create_user(username, email, password)
            context['success'] = True

    return render(request, 'register.html', context)

def logoutView(request):
    logout(request)
    return redirect('homepage:home')