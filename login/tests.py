from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User

class TestLoginRegister (TestCase):
    def test_url_login(self):
        response = self.client.get('/login/')
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, "login.html")

    def test_url_register(self):
        response = self.client.get('/login/register/')
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response, "register.html")

    def test_url_login_yang_sudah_login(self):
        User.objects.create_user(username="aryo", password="aryowira")
        self.client.login(username="aryo", password="aryowira")
        response = self.client.get('/login/')
        self.assertEquals(response.status_code,302)
    
    def test_url_logout(self):
        User.objects.create_user(username="aryo", password="aryowira")
        self.client.login(username="aryo", password="aryowira")
        response = self.client.get('/login/logout')
        self.assertEquals(response.status_code, 301)

    #view

    def test_view_login(self):
        User.objects.create_user(username="aryo", password="aryowira")
        response = self.client.post('/login/', data={
            'username' : 'aryo', 
            'password' : 'aryowira'
        })
        self.assertEquals(response.status_code,302)
        
    def test_view_login_error(self):
        User.objects.create_user(username="aryo", password="aryowira")
        response = self.client.post('/login/', data={
           'username' : 'aryo1', 
           'password' : 'aryowira1'
        })
        self.assertEquals(response.status_code,200)

    def test_view_register(self):
        User.objects.create_user(username="aryo", email= "aryoooo@gmail.com", password="aryowira")
        response = self.client.post('/login/register/', data={
           'username' : 'aryooo',
           'email' : 'aryoooo@gmail.com' ,
           'password' : 'aryowira',
           'password1' : 'aryowira'
        })
        self.assertEquals(response.status_code,200)

    def test_view_register_error(self):
        User.objects.create_user(username="aryo", password="aryowira")
        response = self.client.post('/login/register/', data={
           'username' : 'aryo',
           'email' : 'aryo@gmail.com' ,
           'password' : 'aryowira',
           'password1' : 'aryowiraaaa'
        })
        self.assertEquals(response.status_code,200)
