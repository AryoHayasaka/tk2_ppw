from django.urls import path

from . import views

app_name = 'customer'

urlpatterns = [
    path('', views.customer, name='customer'),
    path('kumpulan-semangat/', views.post_semangat, name='semangat'),
    path('berhasil/', views.post_keluhan, name='keluhan')
]