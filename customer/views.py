import json
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.models import User
from django.core import serializers

from .forms import semangatform, keluhanform
from .models import Semangat, Keluhan

# Create your views here.
def customer(request):
    form_semangat = semangatform()
    form_keluhan = keluhanform()
    response = {
        'user' : request.user,
        'form_semangat': form_semangat,
        'form_keluhan' : form_keluhan,
    }
    return render(request, "customer.html", response)

@login_required
def post_semangat(request):
    if request.user.is_authenticated :
        if request.is_ajax and request.method == 'POST':
            Semangat.objects.create(nama=request.POST['nama'], pesan=request.POST['pesan'])

    daftar_semangat = Semangat.objects.all()
    jsonData = serializers.serialize('json', daftar_semangat)
    data = json.loads(jsonData)
    return JsonResponse(data, safe=False)

@login_required
def post_keluhan(request):
    if request.user.is_authenticated :
        if request.is_ajax and request.method == 'POST':
            Keluhan.objects.create(pesan=request.POST['pesan'])
    daftar_keluhan = Keluhan.objects.all()
    data = serializers.serialize('json', daftar_keluhan)
    dataJson = json.loads(data)
    return JsonResponse(dataJson, safe=False)