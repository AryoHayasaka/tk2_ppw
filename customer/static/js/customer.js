$( document ).ready( function() {
    //untuk menampilkan form
    $("#pesan").click( function() {
        $("#formPesan").toggle('fast');
    });
    $("#keluhan").click( function() {
        $("#formKeluhan").toggle('fast');
    });

    //untuk menampilkan kumpulan semangat dan tentang kami
    $("#kumpulanSemangat").click( function() {
        $("#berkumpul").toggle('fast');
    });
    $("#tentangKami").click( function() {
        $("#isiKami").toggle('fast');
    });

    //untuk fokus saat isi form
    $("#isiNama").focus( function() {
        $(this).css("background-color", "#FDEDEF");
    });
    $("#isiNama").blur( function() {
        $(this).css("background-color", "#FFFFFF");
    });
    $("#isiSemangat").focus( function() {
        $(this).css("background-color", "#FDEDEF");
    });
    $("#isiSemangat").blur( function() {
        $(this).css("background-color", "#FFFFFF");
    });
    $("#isiKeluhan").focus( function() {
        $(this).css("background-color", "#FDEDEF");
    });
    $("#isiKeluhan").blur( function() {
        $(this).css("background-color", "#FFFFFF");
    });

    //untuk menampilkan deskripsi foto
    $('#aryo').mouseover(function () {
        $('#innerAryo').css('visibility','visible');
    })
    $('#aryo').mouseout(function () {
        $('#innerAryo').css('visibility','hidden');
    })

    $('#aye').mouseover(function () {
        $('#innerAye').css('visibility','visible');
    })
    $('#aye').mouseout(function () {
        $('#innerAye').css('visibility','hidden');
    })

    $('#hasna').mouseover(function () {
        $('#innerHasna').css('visibility','visible');
    })
    $('#hasna').mouseout(function () {
        $('#innerHasna').css('visibility','hidden');
    })

    $('#hilmy').mouseover(function () {
        $('#innerHilmy').css('visibility','visible');
    })
    $('#hilmy').mouseout(function () {
        $('#innerHilmy').css('visibility','hidden');
    })

    //untuk handle saat isi form pesan penyemangat
    $("#formMengisiPesan").on( "submit", function(e) {
 
        var formData = JSON.parse(JSON.stringify(jQuery(this).serializeArray()))

            $.ajax({
            type: "POST",
            url: "kumpulan-semangat/",
            data: formData,
            success: function (response) {
                $('#success').css('display', 'block').fadeOut(20000);
                var result = '';
                var nama = response[response.length-1].fields.nama;
                var pesan = response[response.length-1].fields.pesan;

                result += "<table><tbody><tr><td style = 'font-weight : bold'>Dari : " + nama + 
                "</td></tr><br><tr><td>" + pesan + "</td></tr><br><br></tbody></table>";
               
                $('#result').append(result);
            }});
        
            e.preventDefault();
    });

    //untuk handle saat isi form keluh kesah (kritik saran)
    $("#formMengisiKritik").on( "submit", function(e) {
 
        var formData = JSON.parse(JSON.stringify(jQuery(this).serializeArray()))

            $.ajax({
            type: "POST",
            url: "berhasil/",
            data: formData,
            success: function (response) {
                $('#successKritik').css('display', 'block').fadeOut(20000);
            }});
        
            e.preventDefault();
    });
});