
$(document).on('submit', '#form', function(event){
    var form_id = $("#form")
    $.ajax({
        url     : "/dataemail/kirimemail/",
        type    : "POST",
        data    : form_id.serialize(),
        dataType : "json",
        header  : {'X-CSRFToken' : '{% csrf_token%}'},
        success : function(response){
            var success = response['success']
            if (success){
                alert('Email telah terkirim ke semua email yang terdaftar di CoronaStudy!')
            }
        }
    })
    event.preventDefault();
})