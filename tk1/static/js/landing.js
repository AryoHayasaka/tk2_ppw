$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

$(document).on('submit', '#form', function(event){
    event.preventDefault();
    var form_id = $("#form")
    $.ajax({
        url     : "",
        type    : "POST",
        data    : form_id.serialize(),
        dataType : "json",
        header  : {'X-CSRFToken' : '{% csrf_token%}'},
        success : function(response){
            var success = response['success']
            if (success){
                alert ("Email Anda telah terdaftar ! Email konfirmasi dari kami sudah terkirim ke email Anda")
            }
        }
    })
})

AOS.init({
  offset: 200, // offset (in px) from the original trigger point
  delay: 0, // values from 0 to 3000, with step 50ms
  duration: 500 // values from 0 to 3000, with step 50ms
});

$(document).ready(function() {
  // console.log("yes");
  $('#gambar1').hover(function(){
    $(this).animate({
      width : "90%",
    },200)
  }, 
  function() {
    $(this).animate({
      width : "80%",
    },200)
  }
  )

  $('#gambar2').hover(function(){
    $(this).animate({
      width : "90%",
    },200)
  }, 
  function() {
    $(this).animate({
      width : "80%",
    },200)
  }
  )

  $('#gambar3').hover(function(){
    $(this).animate({
      width : "90%",
    },200)
  }, 
  function() {
    $(this).animate({
      width : "80%",
    },200)
  }
  )
});