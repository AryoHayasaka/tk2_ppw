from django import forms
from .models import Email,KirimEmail

class emailForm(forms.ModelForm):
    class Meta:
        model = Email
        fields = ['email']

    email_attrs = {
    'type': 'text',
    'class': 'form-email',
    'placeholder':'Masukkan email'
}

    email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
    email.widget.attrs.update({'class':'form-control'})

class kirimForm(forms.ModelForm):
    class Meta:
        model = KirimEmail
        fields = ['subjek','judul','deskripsi']

    subjek_attrs = {
    'type': 'text',
    'class': 'form-subjek',
    'placeholder':'Masukkan subjek email'
}
    judul_attrs = {
    'type': 'text',
    'class': 'form-judul',
    'placeholder':'Masukkan judul email'
}

    deskripsi_attrs = {
    'type': 'text',
    'class': 'form-deskripsi',
    'placeholder':'Masukkan deskripsi email'
}

    subjek = forms.CharField(widget=forms.TextInput(attrs=subjek_attrs))
    judul = forms.CharField(widget=forms.TextInput(attrs=judul_attrs))
    deskripsi = forms.CharField(widget=forms.Textarea(attrs=deskripsi_attrs))
    
    subjek.widget.attrs.update({'class':'form-control'})
    judul.widget.attrs.update({'class':'form-control'})
    deskripsi.widget.attrs.update({'class':'form-control'})