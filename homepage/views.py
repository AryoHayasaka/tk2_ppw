from django.shortcuts import render, redirect
from .forms import emailForm,kirimForm
from .models import Email,KirimEmail
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import auth, User
from django.contrib.auth.decorators import login_required
import json
from django.core import serializers
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse

# Create your views here

def tambahEmail(request):
    if request.user.is_authenticated :
        if request.method == "POST" and request.is_ajax:
            form = emailForm(request.POST)
            data = {}
            if form.is_valid():
                form.save()
                data['success'] = True
                emailnya = form.cleaned_data['email']
                form = emailForm()
                html_content = render_to_string('emailsambutan.html')
                text_content = strip_tags(html_content)
                email = EmailMultiAlternatives('Terima Kasih telah mendaftarkan email !', text_content, settings.EMAIL_HOST_USER, [emailnya])
                email.attach_alternative(html_content,"text/html")
                email.send()
                return HttpResponse(json.dumps(data), content_type = 'application/json')
        else :
            form = emailForm()
            return render(request, 'landing.html', {'form' : form})
    else:
        form = emailForm()
        return render(request, 'landing.html', {'form' : form})

def notifEmail(request):
    return render(request, 'notif.html')

def dataemailperantara(request):
    data = Email.objects.all()
    dataJson = serializers.serialize('json', data)
    data = json.loads(dataJson)
    return JsonResponse(data, safe=False)

def dataemail(request):
    return render(request, 'dataemail.html')

def kirimemail(request):
    if request.method == "POST" and request.is_ajax:
        form = kirimForm(request.POST)
        data = {}
        if form.is_valid():
            form.save()
            data['success'] = True
            listemail = Email.objects.all()
            subjek  = request.POST.get('subjek')
            judul  = request.POST.get('judul')
            deskripsi = request.POST.get('deskripsi')
            html_content = render_to_string('emailcustom.html',{'subjek' :subjek,'judul':judul,'deskripsi':deskripsi})
            text_content = strip_tags(html_content)
            email = EmailMultiAlternatives(
                subjek, text_content, settings.EMAIL_HOST_USER, listemail
            )

            email.attach_alternative(html_content,"text/html")
            email.send()
            return HttpResponse(json.dumps(data), content_type = 'application/json')

    else :
        form = kirimForm()
        return render(request, 'kirimemail.html', {'form' : form})

def tampilanemailperantara(request):
    isiemail = KirimEmail.objects.all()
    isiJson =serializers.serialize('json',isiemail)
    isi = json.loads(isiJson)
    return JsonResponse(isi, safe=False)

def tampilan(request):
    return render(request, 'tampilanemail.html')

