from django.db import models

# Create your models here.
class Email(models.Model):
    email = models.EmailField(max_length=254)

    def __str__ (self):
        return self.email

class KirimEmail(models.Model):
    subjek = models.CharField(max_length= 100)
    judul = models.CharField(max_length=120)
    deskripsi = models.TextField(max_length=255)

    def __str__ (self):
        return self.judul

    

