from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import tambahEmail,notifEmail, dataemail,kirimemail,tampilan, dataemailperantara, tampilanemailperantara
from .models import Email,KirimEmail
from .forms import emailForm, kirimForm
from django.contrib.auth.models import User
import json

class TestEmail(TestCase):

    # landing

    def test_url_landing_tambah_no_login(self):
        response = Client().get('')
        self.assertEquals(200, response.status_code)
        
    def test_url_landing_tambah_login(self):
        User.objects.create_user('Hilmy', 'hilmy@gmail.com', 'hilmyyy')
        self.client.login(username= 'Hilmy', password = 'hilmyyy')
        response = Client().get('')
        self.assertEquals(200, response.status_code)

    def test_view_landing(self):
        User.objects.create_user('Hilmy', 'hilmy@gmail.com', 'hilmyyy')
        self.client.login(username= 'Hilmy', password = 'hilmyyy')
        response = self.client.post('/', data= {'email' : 'test@ppw.com'})
        self.assertEquals(Email.objects.count(), 1)

    def test_form_email(self):
        form = emailForm(data= {'email' : 'test@ppw.com'})
        form.save()
        self.assertEqual(1, Email.objects.all().count())

    def test_model_landing(self):
        User.objects.create_user('Hilmy', 'hilmy@gmail.com', 'hilmyyy')
        self.client.login(username= 'Hilmy', password = 'hilmyyy')
        new_activity = Email.objects.create(email='test@ppw.com')
        count = Email.objects.all().count()
        self.assertEqual(count, 1)
        self.assertEqual(str(new_activity),'test@ppw.com')
    
    # notif
    def test_url_notif(self):
        response = Client().get('/notif/')
        self.assertEquals(200, response.status_code)

    def test_template_notif(self):
        response = Client().get('/notif/')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'notif.html')

    # data email
    def test_url_dataemail(self):
        response = Client().get('/dataemail/')
        self.assertEquals(200, response.status_code)

    def test_url_dataemailperantara(self):
        response = Client().get('/dataemail/dataperantara/')
        self.assertEquals(200, response.status_code)

    def test_template_dataemail(self):
        response = Client().get('/dataemail/')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'dataemail.html')
    
    # kirim email
    def test_url_kirimemail(self):
        response = Client().get('/dataemail/kirimemail/')
        self.assertEquals(200, response.status_code)

    def test_template_kirimemail(self):
        response = Client().get('/dataemail/kirimemail/')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'kirimemail.html')

    def test_model_kirimemail(self):
        new_activity = KirimEmail.objects.create(subjek = 'testsubjek',judul= 'testjudul',deskripsi ='testdeskripsi')
        count = KirimEmail.objects.all().count()
        self.assertEqual(count, 1)
        self.assertEqual(str(new_activity),'testjudul')

    def test_view_kirimemail(self):
        # response = self.client.post(reverse('homepage:kirimemail'), data={'subjek' : 'testsubjek','judul' : 'testjudul','deskripsi' : 'testdeskripsi'})
        self.client.post('/dataemail/kirimemail/', data= {'subjek' : 'testsubjek','judul' : 'testjudul','deskripsi' : 'testdeskripsi'})
        self.assertEquals(KirimEmail.objects.count(), 1)

    # tampilan email
    def test_url_tampilanemail(self):
        response = Client().get('/dataemail/tampilanemail/')
        self.assertEquals(200, response.status_code)
        
    def test_url_tampilanperantara(self):
        response = Client().get('/dataemail/tampilanemail/tampilanperantara/')
        self.assertEquals(200, response.status_code)

    def test_template_tampilanemail(self):
        response = Client().get('/dataemail/tampilanemail/')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'tampilanemail.html')



    


